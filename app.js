var express = require('express')
var path = require('path')
var favicon = require('serve-favicon')
var logger = require('morgan')
var cookieParser = require('cookie-parser')
var bodyParser = require('body-parser')
var robots = require('express-robots-txt')
var projectConfig = require('./modules/config-middleware')
var {flagsMiddleware, generateFlagsFile} = require('./modules/feature-flags')

/* Generate flags.config.json file when servers starts */
generateFlagsFile()

/* =============================
    Routes files declarations
============================= */
// index pages
var landingPage = require('./routes/landing-page/index')
var icons = require('./routes/icons/index')
var dashboard = require('./routes/dashboard/index')
var brandColors = require('./routes/colors/index')
var dashboardWriting = require('./routes/writing/index')
var cookiesPolicy = require('./routes/shared/index')
var changeLog = require('./routes/changelog/index')
var buttons = require('./routes/buttons/index')
var typographyPage = require('./routes/typography/index')
// submenu pages
var howTo = require('./routes/colors/how-to')
var eosIconsSet = require('./routes/icons/eos-icons-set')
var iconsWhenToUseThem = require('./routes/icons/when-to-use-them')
var logosGuide = require('./routes/icons/logo-icons')
var logoIconsSet = require('./routes/icons/logo-icons-set')
var uxWriting = require('./routes/writing/ux-writing')
var brandVoice = require('./routes/writing/brand-voice')
var brandTone = require('./routes/writing/brand-tone')
var conventionsAndRules = require('./routes/writing/conventions-and-rules')
var iconsUx = require('./routes/icons/icon-ux')
var iconButtons = require('./routes/buttons/icon-buttons')
var buttonsSizing = require('./routes/buttons/sizing')
// news
var newsEosIconsV2 = require('./routes/news/eos-icons-v2')
// Internal pages
var iconsReport = require('./routes/internal/icons-report')
var specialIcons = require('./routes/internal/special-icons')
var spacewalkIconsSuma = require('./routes/internal/spacewalk-icons-suma')
var spacewalkIconsSCC = require('./routes/internal/spacewalk-icons-scc')
var stratosIcons = require('./routes/internal/stratos-icons')
var designSpecs = require('./routes/internal/design-specs')
var mockedMenu = require('./routes/internal/mocked-menu')

// API
var iconsAPI = require('./routes/api/icons/index')
var gitlabAPI = require('./routes/api/gitlab/index')
var feedbackTool = require('./routes/api/feedback-tool/index')
var featureFlags = require('./routes/api/feature-flags/index')
var logoIcons = require('./routes/api/logo-icons/index')

var app = express()

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'pug')

app.use(favicon(path.join(__dirname, 'assets/images', 'favicon.png')))
app.use(logger('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'assets')))
app.use(express.static(path.join(__dirname, 'vendors')))
app.use(projectConfig)
app.use(flagsMiddleware)

/* Set public folder for sitemaps, pdf etc.. */
app.use(express.static(path.join(__dirname, 'public')))
/* Set robots.txt with express middleware, for the sitemap, place it inside /public folder */
if (process.env.NODE_ENV === 'development' || process.env.NODE_ENV === 'staging' || process.env.NODE_ENV === undefined) {
  /* Disallow bots for development/staging */
  app.use(robots({UserAgent: '*', Disallow: '/'}))
}
if (process.env.NODE_ENV === 'production') {
  /* Allow bots for production */
  app.use(robots({UserAgent: '*', Disallow: ['/cookies-policy'], Sitemap: '/sitemap.xml'}))
}

/* =============================
       Routes definitions
============================= */
// index pages routes
app.use('/', landingPage)
app.use('/dashboard', dashboard)
app.use('/icons', icons)
app.use('/colors', brandColors)
app.use('/writing', dashboardWriting)
app.use('/cookies-policy', cookiesPolicy)
app.use('/changelog', changeLog)
app.use('/buttons', buttons)
app.use('/typography', typographyPage)
// submenu routes
app.use('/colors/how-to', howTo)
app.use('/icons/when-to-use-them', iconsWhenToUseThem)
app.use('/icons/eos-icons-set', eosIconsSet)
app.use('/icons/icon-ux', iconsUx)
app.use('/icons/logo-icons', logosGuide)
app.use('/icons/logo-icons-set', logoIconsSet)
app.use('/writing/ux-writing', uxWriting)
app.use('/writing/brand-voice', brandVoice)
app.use('/writing/brand-tone', brandTone)
app.use('/writing/conventions-and-rules', conventionsAndRules)
app.use('/internal/special-icons', specialIcons)
app.use('/internal/design-specs', designSpecs)
app.use('/internal/stratos-icons', stratosIcons)
app.use('/internal/icons-report', iconsReport)
app.use('/internal/spacewalk-icons-suma', spacewalkIconsSuma)
app.use('/internal/spacewalk-icons-scc', spacewalkIconsSCC)
app.use('/internal/mocked-menu', mockedMenu)
app.use('/buttons/icon-buttons', iconButtons)
app.use('/buttons/sizing', buttonsSizing)

// news routes
app.use('/news/eos-icons-v2', newsEosIconsV2)
// API
app.use('/api/icons', iconsAPI)
app.use('/api/gitlab', gitlabAPI)
app.use('/api/feedback', feedbackTool)
app.use('/api/feature-flags', featureFlags)
app.use('/api/logo-icons', logoIcons)

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('Not Found')
  err.status = 404
  next(err)
})

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function (err, req, res, next) {
    res.status(err.status || 500)
    res.render('error', {
      message: err.message,
      error: err
    })
  })
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
  res.status(err.status || 500)
  res.render('error', {
    message: err.message,
    error: {}
  })
})

module.exports = app
