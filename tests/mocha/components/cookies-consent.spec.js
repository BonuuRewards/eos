/* Unit test for cookies consent.
  ========================================================================== */
describe('Cookies consent', () => {
  const cookiesBanner= $("<div class='js-cookies-alert'></div>")

  before((done) => {
    $('body').append(cookiesBanner)
    done()
  })

  after((done) => {
    $(cookiesBanner).remove()
    done()
  })

  it('Should make the banner visible.', () => {
    cookieController()
    expect(cookiesBanner).to.have.css('visibility', 'visible')
  })

  it('Should make the banner go away.', () => {
    // @TODO
    // the acceptance of the cookie can be tested, by checking that the banner goes away
    // this will require some re-factoring in the code of the controller as the
    // cookieController.acept is not accesible outside of the controller
  })
})
