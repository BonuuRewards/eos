/* Unit test for the Feedback tool
  ========================================================================== */
var resetFeedback = sinon.fake()
var showSuccess = sinon.fake()
var sendForm = sinon.fake()

describe('Feedback tool', function () {

  describe('parseFormContent', function () {
    var screenshotCheckbox = $('<input class="js-feedback-tool-screenshoot-trigger" type="checkbox"></input>')

    beforeEach(function (done) {
      $('body').append(screenshotCheckbox)
      done()
    })

    afterEach(function (done) {
      $(screenshotCheckbox).remove()
      done()
    })

    it('Should return an object with the message sent and an image', function () {
      var message = 'something'
      attachment = 'base64 image code'
      // tick the checkbox
      $(screenshotCheckbox).prop('checked', true)
      // parse content
      var response = parseFormContent(message)
      expect(response.img).to.equal(attachment)
      expect(response.message).to.equal(message)
    })

    it('Should return an object with null message and image', function () {
      // make sure the checkbox is not ticked
      $(screenshotCheckbox).prop('checked', false)
      // parse content sending no argument
      var response = parseFormContent()
      expect(response).to.have.keys(['message', 'img'])
      expect(response.img).to.be.null
      expect(response.message).to.be.null
    })
  })

  describe('feedbackFormActions', function () {
    var feedbackMessage = $('<textarea class="js-feedback-tool-message"></textarea>')

    beforeEach(function (done) {
      $('body').append(feedbackMessage)
      done()
    })

    afterEach(function (done) {
      $(feedbackMessage).remove()
      done()
    })

    it('Should reset the feedback component when finishing feedback', function (done) {
      feedbackFormActions.finishAction()
      // since there is an animation going on, we need to wait for it to finish
      setTimeout(function () {
        expect(resetFeedback).called
        done()
      }, 601);
    })

    it('Should send the form when there is enough text', function () {
      // Create spies
      var spy = sinon.spy(window, 'parseFormContent')

      // Insert some text into the input field
      var message = 'This is a test message'
      $('.js-feedback-tool-message').val(message)
      // trigger the unit under test
      validateForm()
      // test that it called the parseFormContent
      expect(spy).calledWith(message)
      // test that it return true, which means it called sendForm()
      expect(sendForm).calledOnce
      expect(validateForm()).to.be.true
    })

    it('Should not send the form when there is no text', function () {
      $('.js-feedback-tool-message').val('')
      expect(validateForm()).to.be.false
    })
  })
})
