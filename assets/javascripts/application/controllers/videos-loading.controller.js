/* Initial object with our videos URL */
const videosObject = {
  eosIcons: {
    title: 'EOS - Icons guides',
    url: 'https://res.cloudinary.com/eosdesignsystem/video/upload/c_scale,vc_auto,w_1024/v1533300617/EOS/videos/eos-icons-guides.mp4'
  },

  eosWritinng: {
    title: 'EOS - Writing guides',
    url: 'https://res.cloudinary.com/eosdesignsystem/video/upload/c_scale,h_576,vc_auto,w_1024/v1532004547/EOS/videos/writing_guides_s.mp4'
  }
}

$(function () {
  $('.js-launch-modal').on('click', function () {
    /* Check which video we should load  */
    const videoToLoad = $(this).attr('data-video-name')

    /* Add the src attribute to the video element */
    $('.js-eos-icons').attr('src', videosObject[videoToLoad].url)
    /* This is needed for "load" the video once the src is set */
    $('.js-video-modal video')[0].load()
  })
})
