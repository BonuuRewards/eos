/* Source: https://github.com/kenwheeler/slick/
   ========================================================================== */
$(document).ready(() => {
  $('.js-news-tutorials-carousel').slick({
    infinite: false,
    speed: 300,
    slidesToShow: 2,
    slidesToScroll: 1,
    prevArrow: '.js-news-tutorials-before',
    nextArrow: '.js-news-tutorials-next',
    responsive: [
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1
        }
      }
    ]
  })
})
