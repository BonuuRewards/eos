$(document).on('ready', function () {
  /* When clicking on one of the products labels in the filter */
  $('.js-filter-product-icons').on('click', function () {
    const filterName = $(this)
      .clone()
      .children()
      .remove()
      .end()
      .text()
    filterProductsOut(filterName)
  })
  $('.js-filter-version-icons').on('click', function () {
    const filterName = $(this)
      .clone()
      .children()
      .remove()
      .end()
      .text()
    filterVersionsOut(filterName)
  })
  $('.js-filter-reported-icons').on('click', function () {
    const filterName = $(this)
      .clone()
      .children()
      .remove()
      .end()
      .text()
    filterReportedOut(filterName)
  })
  countIconsProducts()
  countIconsVersion()
})

const filterProductsOut = (label) => {
  const _iconsContainer = $('.icons-list article')
  for (let i = 0; i < _iconsContainer.length; i++) {
    const filterLabel = $(_iconsContainer[i]).find('.js-filter-label').text()
    if (filterLabel === label || label === 'All') {
      _iconsContainer[i].style.display = ''
    } else {
      _iconsContainer[i].style.display = 'none'
    }
  }
}

const filterVersionsOut = (label) => {
  const _iconsContainer = $('.icons-list article')
  for (let i = 0; i < _iconsContainer.length; i++) {
    const filterLabel = $(_iconsContainer[i]).find('.js-filter-version-label').text()
    if (filterLabel === label || label === 'All') {
      _iconsContainer[i].style.display = ''
    } else {
      _iconsContainer[i].style.display = 'none'
    }
  }
}

const filterReportedOut = (label) => {
  const _iconsContainer = $('.icons-list article')
  for (let i = 0; i < _iconsContainer.length; i++) {
    const filterLabel = $(_iconsContainer[i]).find('.js-filter-reported-label').text()
    if (filterLabel === label || label === 'All') {
      _iconsContainer[i].style.display = ''
    } else {
      _iconsContainer[i].style.display = 'none'
    }
  }
}

const countIconsVersion = () => {
  const _allProducts = $('.js-count-version-icons')
  $.each(_allProducts, function (i, v) {
    const productName = $(v)
      .parent()
      .clone()
      .children()
      .remove()
      .end()
      .text()
    const _counterContainer = $(v)
    const _iconsContainer = $(`.icons-list article .js-filter-version-label:contains(${productName})`)
    const countIcons = _iconsContainer.length
    _counterContainer.html(countIcons)
  })
}

const countIconsProducts = () => {
  const _allProducts = $('.js-count-product-icons')
  $.each(_allProducts, function (i, v) {
    const productName = $(v)
      .parent()
      .clone()
      .children()
      .remove()
      .end()
      .text()
    const _counterContainer = $(v)
    const _iconsContainer = $(`.icons-list article .js-filter-version-label:contains(${productName})`)
    const countIcons = _iconsContainer.length
    _counterContainer.html(countIcons)
  })
}
