const getIconsCollectionService = (callback) => { // eslint-disable-line no-unused-vars
  $.when(
    $.ajax({
      url: `/node_modules/eos-icons/dist/js/glyph-list.json`,
      dataType: `json`,
      error: function (xhr, status, error) {
        console.log(`there was an error retrieving the icons collection`)
      }
    }),
    $.ajax({
      url: '/node_modules/eos-icons/package.json',
      dataType: 'json',
      error: function (xhr, status, error) {
        console.log(`there was an error retrieving the icons collection`)
      }
    }))
    .then(function (dataGlyph, dataPackage) {
      callback(dataGlyph, dataPackage)
    })
}
