const fs = require('fs')
const fetch = require('node-fetch')

// You can also manually update the config file at: http://localhost:3000/api/features */
const generateFlagsFile = async () => {
  /* We get the data by fetching unleash server api */
  const getData = async () => {
    /* Request config */
    const unleash = 'https://unleash-eos.herokuapp.com/api/client/features/'

    /* Fetching data from endpoint */
    const request = await fetch(unleash)
    return await request.json()
  }

  /* Store data from the requests in data */
  const data = await getData()

  /* Only write the file if the requests is resolve, if the request is reject we ignore this */
  if (data.version) {
    fs.writeFile('features.config.json', JSON.stringify(data, null, 2), (err) => {
      /* Display any errors */
      if (err) console.log(err);
      console.log('Generated new features.config.json file!');
    })
  } else  {
    return console.log(`There was an error fetching the API`)
  }
}

/* Middleware for routes that check if we should allow or not the request based on features flags. */
const flagsMiddleware = async (req, res, next) => {
  const readConfig = fs.readFileSync('features.config.json', 'utf-8')
  const flag = req.originalUrl.substr(1).split('/').pop()

  // We convert the response json to an object so we can work with it.
  const flagsData =  await JSON.parse( readConfig )

  /* ========  Use this to local development with the production behaviour  ========  */
  // const flagsData = await JSON.parse(readConfig)

  /* Reduce the data array to an object, also if it's development we enable enverthing */
  const flagsStatus = flagsData.features.reduce((acc, cur) => {
    acc[cur.name] = {
      enabled: process.env.NODE_ENV !== 'development' ? cur.enabled : true
    }

    return acc
  }, {})

  res.locals.flags = flagsStatus

  /* Here we decide what we do with the request */
  if(flagsStatus[flag]) {
    if (flagsStatus[flag] && flagsStatus[flag].enabled) {
      /* If the flags exists and is enabled, we allow the route */
      next()
    } else {
    /* If the flags exists and is disable, we redirect to dashboard */
      res.redirect('/dashboard')
    }
  } else {
    /* If the flag is non-existent, we ignore the comprobation */
    next()
  }
}

module.exports = {
  flagsMiddleware,
  generateFlagsFile
}
