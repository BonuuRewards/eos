var express = require('express')
var router = express.Router()
var fs = require('fs')

/* ==========================================================================
   Generate a single object from icon-svg folder
   ========================================================================== */

const dir = `assets/images/logo-icons/`
let dataLogoIcon = []

dataLogoIcon = fs.readdirSync(dir)
router.get('/', function (req, res, next) {
  res.status(200).json(dataLogoIcon)
})

module.exports = router
