var express = require('express')
var router = express.Router()

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('shared/legal/cookies-policy', {
    title: 'Cookies Policy',
    description: '',
    path: req.originalUrl
  })
})

module.exports = router
